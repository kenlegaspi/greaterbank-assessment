package com.greater.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.greater.bank.config.ApplicationConfiguration;
import com.greater.bank.services.FileDirectoryService;

@Component
public class ApplicationStartupEvent {

    private static final Logger log = LoggerFactory.getLogger(ApplicationStartupEvent.class);
 
    @Autowired
    private FileDirectoryService fileDirectoryService;
    
	@Autowired
	private ApplicationConfiguration appconfig;
	
    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {        
        log.info("Creating Initial Folders");
        
        fileDirectoryService.createDirectory(appconfig.getDirectoryPending());
    }
}
