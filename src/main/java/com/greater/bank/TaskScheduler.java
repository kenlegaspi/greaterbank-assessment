package com.greater.bank;

import java.io.File;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.greater.bank.config.ApplicationConfiguration;
import com.greater.bank.services.FileDirectoryService;

@Component
public class TaskScheduler {
	
	private static final Logger log = LoggerFactory.getLogger(TaskScheduler.class);
	
	@Autowired
	private FileDirectoryService fileDirectoryService; 
	
	@Autowired
	private ApplicationConfiguration appconfig;
	
	@Scheduled(cron = "*/10 0-5 06,21 * * *")
	public void processTransactions() {
		log.info("Application Scheudled Run on " + LocalDateTime.now());
		
		File directory = fileDirectoryService.createDirectory(appconfig.getDirectoryPending());
		
		fileDirectoryService.readFilesFromDirectory(directory);

	}

}
