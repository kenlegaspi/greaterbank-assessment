package com.greater.bank.services;

import java.io.File;

public interface FileDirectoryService {

	public File createDirectory(String directoryName);
	public void readFilesFromDirectory(File directory);
}
