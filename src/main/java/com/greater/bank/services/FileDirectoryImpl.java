package com.greater.bank.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greater.bank.config.ApplicationConfiguration;
import com.greater.bank.config.Constants;
import com.greater.bank.model.CustomerTransactionBean;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

@Service
public class FileDirectoryImpl implements FileDirectoryService {

	private static final Logger log = LoggerFactory.getLogger(FileDirectoryImpl.class);

	@Autowired
	private ApplicationConfiguration appconfig;
	
	@Override
	public File createDirectory(String directoryName) {
				
		//check if $TRANSACTION_PROCESSING is set in the environment
		String transactionProcessing = System.getenv(Constants.TRANSACTION_PROCESSING);
			
		//if $TRANSACTION_PROCESSING is not set in the env, create file in the user directory instead
		if (null == transactionProcessing) {			
			transactionProcessing = System.getProperty(appconfig.getUserDirectory());										
		}
				
		File directory = new File(transactionProcessing + "/" + directoryName);

		if (!directory.isDirectory()) {
			directory.mkdir();

			log.info("File Directory Created at: " + directory.toString());
		}

		return directory;
	}

	@Override
	public void readFilesFromDirectory(File directory) {
		
		File[] files = directory.listFiles();
		
		for (File f : files) {
			String content = readFileContent(f);
			String fileName = f.getName();
			LocalDateTime localTime = LocalDateTime.now();
			
			boolean hasInvalidFileName = false;

			try {
				
				//check if filename is in the format : finance_customer_transactions-${datetime}.csv
				localTime = LocalDateTime.parse(f.getName().split("-")[1].toString().replace(".csv", ""),
						DateTimeFormatter.ofPattern(appconfig.getDateFormat()));
				
			} catch (Exception e) {
				log.info("Rewriting filename to make use of the current date and time");
				log.info("Old filename: " + fileName);
				
				hasInvalidFileName = true;
				
				fileName = appconfig.getDirectoryFileName() + "-"
						+ localTime.format(DateTimeFormatter.ofPattern(appconfig.getDateFormat())) + ".csv";

				log.info("New filename: " + fileName);
			}

			//process files found in $TRANSACTION_PROCESSING/pending
			processFiles(f, content, fileName, hasInvalidFileName);
		}
	}

	private void processFiles(File f, String content, String fileName, boolean hasInvalidFileName) {
		try {

			File newFile = new File(createDirectory(appconfig.getDirectoryProcessed()).getPath() + "/" + fileName);

			if (newFile.exists() && hasInvalidFileName) {

				log.info("Append to existing file : " + fileName);

				Files.write(Paths.get(newFile.toPath().toUri()), readSkipFirstLine(content).getBytes(),
						StandardOpenOption.APPEND);

				processValidTransactions(newFile);

			} else {
				
				log.info("Create new file with filename: " + fileName);

				Files.write(Paths.get(newFile.toPath().toUri()), content.getBytes(), StandardOpenOption.CREATE);

				processValidTransactions(newFile);
			}

			deleteInputFileFromServer(f);

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private String readFileContent(File f) {
		String content = null;
		Path path = Paths.get(f.toPath().toUri());

		try {
			content = new String(Files.readAllBytes(path));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return content;
	}

	private String readSkipFirstLine(String content) {
		BufferedReader reader = new BufferedReader(new StringReader(content));
		String line, newContent = "";

		try {
			reader.readLine();
			while ((line = reader.readLine()) != null) {
				newContent += line + "\n";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return newContent;
	}
	
	public void processValidTransactions(File file) {
		
		/*
		 * BeanListProcessor converts each parsed row to an instance of a given class,
		 * then stores each instance into a list.
		*/
		BeanListProcessor<CustomerTransactionBean> rowProcessor = new BeanListProcessor<CustomerTransactionBean>(
				CustomerTransactionBean.class);

		parseCsvFile(file, rowProcessor);

		// The BeanListProcessor provides a list of objects extracted from the input.
		List<CustomerTransactionBean> filteredList = rowProcessor.getBeans().stream()
				.filter(line -> line.getCustomerAccount().matches("^[0-9]*$")).collect(Collectors.toList());

		double totalCreditTransactions = 0, totalDebitTransactions = 0;

		for (CustomerTransactionBean b : filteredList) {
			if (b.getTransactionAmount() > 0) {
				totalCreditTransactions += b.getTransactionAmount();
			} else {
				totalDebitTransactions += b.getTransactionAmount();
			}
		}

		int skippedTransactions = rowProcessor.getBeans().size() - filteredList.size();
		int totalAccounts = rowProcessor.getBeans().size();

		createReport(file.getName(), totalAccounts, totalCreditTransactions, totalDebitTransactions,
				skippedTransactions);
	}

	private void parseCsvFile(File file, BeanListProcessor<CustomerTransactionBean> rowProcessor) {
		CsvParserSettings parserSettings = new CsvParserSettings();
		parserSettings.setProcessor(rowProcessor);
		parserSettings.setHeaderExtractionEnabled(true);

		CsvParser parser = new CsvParser(parserSettings);

		parser.parse(file);
	}

	public void createReport(String fileName, Integer totalAccounts, Double totalCreditTransactions,
			Double totalDebitTransactions, Integer skippedTransactions) {

		File newFile = new File(
				createDirectory(appconfig.getDirectoryReports()).getPath() + "/" + fileName.replace("csv", "txt"));

		StringBuffer content = new StringBuffer().append(appconfig.getFileProcessed() + " : " + fileName + "\n")
				.append(appconfig.getTotalAccounts() + " : " + totalAccounts + "\n")
				.append(appconfig.getTotalCreditTransactions() + " : " + formatToLocalCurrency(totalCreditTransactions)
						+ "\n")
				.append(appconfig.getTotalDebitTransactions() + " : "
						+ formatToLocalCurrency(Math.abs(totalDebitTransactions)) + "\n")
				.append(appconfig.getTotalSkippedTransactions() + " : " + skippedTransactions);

		try {
			Files.write(Paths.get(newFile.toPath().toUri()), content.toString().getBytes(), StandardOpenOption.CREATE);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String formatToLocalCurrency(Double number) {
		NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("en", "AU"));
		String currency = format.format(number);

		return currency;
	}

	public void deleteInputFileFromServer(File file) {
		if (file.delete()) {
			log.info(file.getName() + " is deleted from server");
		} else {
			log.info("File has already been deleted in the server");
		}
	}
}
