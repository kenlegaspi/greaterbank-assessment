package com.greater.bank.model;

import com.univocity.parsers.annotations.Parsed;

public class CustomerTransactionBean {

	@Parsed(index = 0)
	private String customerAccount;
	
	@Parsed(index = 1)
	private Double transactionAmount;

	public String getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(String customerAccount) {
		this.customerAccount = customerAccount;
	}

	public Double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(Double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
}
