package com.greater.bank.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:config.properties")
public class ApplicationConfiguration {

    @Autowired
    private Environment env;
    
    public String getDirectoryPending() {
		return env.getProperty("directory.pending");
	}

    public String getDirectoryProcessed() {
    		return env.getProperty("directory.processed");
    }
    
    public String getDirectoryReports() {
    		return env.getProperty("directory.reports");
    }
    
    public String getDirectoryFileName() {
    		return env.getProperty("directory.filename");
    }
    
    public String getDateFormat() {
    		return env.getProperty("date.format");
    }
    
    public String getTransactionProcessing() {
    		return env.getProperty("transaction.processing");
    }

    public String getUserDirectory() {
    		return env.getProperty("transaction.user.dir");
    }
    
    public String getFileProcessed() {
		return env.getProperty("file.processed");
    }

    public String getTotalAccounts() {
		return env.getProperty("file.total.accounts");
    }
    		
    public String getTotalCreditTransactions() {
		return env.getProperty("file.total.credit.transactions");
    }
    	
    public String getTotalDebitTransactions() {
		return env.getProperty("file.total.debit.transactions");
    }
    
    public String getTotalSkippedTransactions() {
		return env.getProperty("file.total.skipped.transactions");
    }    
}
